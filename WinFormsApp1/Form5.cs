﻿using System.Data.SqlClient;
using System;
using System.Data;
using System.Windows.Forms;
using System.Configuration;

namespace WinFormsApp1
{
    public partial class Form5 : Form
    {
        public Form5()
        {
            InitializeComponent();
            comboBox_1();
            comboBox_2();


        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd = new SqlCommand("Insert into RubricLevel values (@RubricId,@Details,@MeasurementLevel) ", con);
            SqlCommand cmd1 = new SqlCommand("Select Id from Rubric where Id= '" + rubId.SelectedItem + "' ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            da.Fill(dt);
            int id = Convert.ToInt32(dt.Rows[0].ItemArray[0].ToString());

            cmd.Parameters.AddWithValue("@RubricId", id);
            cmd.Parameters.AddWithValue("@Details", Det.Text);
            cmd.Parameters.AddWithValue("@MeasurementLevel", mel.Text);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved");
        }
        public void comboBox_1()
        {
            rubId.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Rubric", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                rubId.Items.Add(dt.Rows[i].ItemArray[0]);
            }
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 f = new Form1();
            f.Show();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var conDel = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete from RubricLevel Where Id='" + rublvlId.SelectedItem     + "'", conDel);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully deleted");
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from RubricLevel", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }
        public void comboBox_2()
        {
            rublvlId.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from RubricLevel", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                rublvlId.Items.Add(dt.Rows[i].ItemArray[0]);
            }
        }

        private void button5_Click(object sender, EventArgs e)
        {
            comboBox_2();
        }
    }   
}
