﻿using System.Data.SqlClient;
using System;
using System.Data;
using System.Windows.Forms;
using System.Configuration;

namespace WinFormsApp1
{
    public partial class Form3 : Form
    {
        public Form3()
        {
            InitializeComponent();
            comboBox_1();
        }
        DateTime newDate;
        private void button5_Click(object sender, EventArgs e)
        {

        }


        private void button1_Click_1(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd = new SqlCommand("Insert into Clo values (@Name, @DateCreated, @DateUpdated)", con);
            cmd.Parameters.AddWithValue("@Name", statement.Text);
            cmd.Parameters.AddWithValue("@DateCreated", newDate);
            cmd.Parameters.AddWithValue("@DateUpdated", newDate);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved");
            string op = MessageBox.Show("Want to add rubric here?", "Rubric insertion", MessageBoxButtons.YesNo).ToString();

            if (op == "Yes")
            {
                this.Hide();
                Form2 f = new Form2();
                f.Show();
            }
            else
            {

            }
        }

        private void button2_Click_1(object sender, EventArgs e)
        {
            var conDel = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete from Clo Where Name='" + statement.Text + "'", conDel);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully deleted");
        }

        private void button4_Click_1(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Clo", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }


        private void button5_Click_1(object sender, EventArgs e)
        {
            newDate = DateTime.Now;
            MessageBox.Show("Date added");
        }

        private void statement_TextChanged(object sender, EventArgs e)
        {

        }

        private void button6_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Clo", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView2.DataSource = dt;
        }
        public void comboBox_1()
        {
            comboBox1.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Clo", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                comboBox1.Items.Add(dt.Rows[i].ItemArray[0]);
            }
        }
        private void button3_Click(object sender, EventArgs e)
        {
            newDate = DateTime.Now;
            MessageBox.Show("Date added");
        }

        private void button7_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            if (!string.IsNullOrEmpty(comboBox1.SelectedItem.ToString()))
            {

                SqlCommand cmd = new SqlCommand("Update Clo Set Name=@Name,DateUpdated=@DateUpdated Where Id='" + comboBox1.SelectedItem + "'", con);

                cmd.Parameters.AddWithValue("@Name", name.Text);
                cmd.Parameters.AddWithValue("@DateUpdated", newDate);
                cmd.ExecuteNonQuery();


                MessageBox.Show("Data Updated Successfully");
            }
            else
            {
                MessageBox.Show("Please Enter Clo Id...");
            }
        }

        private void button8_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 f1 = new Form1();
            f1.Show();
        }
    }
}
