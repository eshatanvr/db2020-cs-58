﻿using System.Data.SqlClient;
using System;
using System.Data;
using System.Windows.Forms;
using System.Configuration;

namespace WinFormsApp1
{
    public partial class Form4 : Form
    {
        public Form4()
        {
            InitializeComponent();
            comboBox_1();
            comboBox2();
            comboBox3();
        }

        private void Form4_Load(object sender, EventArgs e)
        {

        }

        private void textBox5_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox4_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox3_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox2_TextChanged(object sender, EventArgs e)
        {

        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void label6_Click(object sender, EventArgs e)
        {

        }

        private void label5_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd = new SqlCommand("Insert into Student values (@FirstName, @LastName, @Contact,@Email,@RegistrationNumber,@Status)", con);
            cmd.Parameters.AddWithValue("@FirstName", name.Text);
            cmd.Parameters.AddWithValue("@LastName", lastName.Text);
            cmd.Parameters.AddWithValue("@Contact", cont.Text);
            cmd.Parameters.AddWithValue("@Email", mail.Text);
            cmd.Parameters.AddWithValue("@RegistrationNumber", rg.Text);
            SqlCommand cmd1 = new SqlCommand("Select LookupId from lookUp where Category='STUDENT_STATUS' and Name='"+status.SelectedItem+"'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            da.Fill(dt);
            int lookid = Int32.Parse(dt.Rows[0].ItemArray[0].ToString());
            cmd.Parameters.AddWithValue("@Status", lookid);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved");
        }
        public void comboBox_1()
        {
            status.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Name from lookUp where Category='STUDENT_STATUS'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                status.Items.Add(dt.Rows[i].ItemArray[0]);
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button4_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 f = new Form1();
            f.Show();
        }

        private void textBox5_TextChanged_1(object sender, EventArgs e)
        {

        }
        public void comboBox2()
        {
            stdId.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Student", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                stdId.Items.Add(dt.Rows[i].ItemArray[0]);
            }
        }

        public void comboBox3()
        {
            sts.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Name from lookUp where Category='STUDENT_STATUS'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                sts.Items.Add(dt.Rows[i].ItemArray[0]);
            }
        }

        private void button3_Click(object sender, EventArgs e)
        {

            var conDel = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Delete from Student Where Id='" + stdId.SelectedItem + "'", conDel);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully deleted");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            if (!string.IsNullOrEmpty(stdId.SelectedItem.ToString()))
            {
                if (!string.IsNullOrEmpty(fname.Text.ToString()))
                {
                    SqlCommand cmd = new SqlCommand("Update Student Set FirstName=@FirstName Where Id='" + stdId.SelectedItem + "'", con);

                    cmd.Parameters.AddWithValue("@FirstName", fname.Text);
                    cmd.ExecuteNonQuery();


                    MessageBox.Show("First name Updated Successfully");
                }
                if (!string.IsNullOrEmpty(lname.Text.ToString()))
                {
                    SqlCommand cmd = new SqlCommand("Update Student Set LastName=@LastName Where Id='" + stdId.SelectedItem + "'", con);

                    cmd.Parameters.AddWithValue("@LastName", lname.Text);
                    cmd.ExecuteNonQuery();


                    MessageBox.Show("last name Updated Successfully");
                }
                if (!string.IsNullOrEmpty(c.Text.ToString()))
                {
                    SqlCommand cmd = new SqlCommand("Update Student Set Contact=@Contact Where Id='" + stdId.SelectedItem + "'", con);

                    cmd.Parameters.AddWithValue("@Contact", c.Text);
                    cmd.ExecuteNonQuery();


                    MessageBox.Show("Contact Updated Successfully");
                }
                if (!string.IsNullOrEmpty(email.Text.ToString()))
                {
                    SqlCommand cmd = new SqlCommand("Update Student Set Email=@Email Where Id='" + stdId.SelectedItem + "'", con);

                    cmd.Parameters.AddWithValue("@Email", email.Text);
                    cmd.ExecuteNonQuery();


                    MessageBox.Show("Email Updated Successfully");
                }
                if (!string.IsNullOrEmpty(rgNo.Text.ToString()))
                {
                    SqlCommand cmd = new SqlCommand("Update Student Set RegistrationNumber=@RegistrationNumber Where Id='" + stdId.SelectedItem + "'", con);

                    cmd.Parameters.AddWithValue("@RegistrationNumber", rgNo.Text);
                    cmd.ExecuteNonQuery();


                    MessageBox.Show("Registration Number Updated Successfully");
                }
                if (!string.IsNullOrEmpty(sts.SelectedItem.ToString()))
                {
                    SqlCommand cmd = new SqlCommand("Update Student Set Status=@Status Where Id='" + stdId.SelectedItem + "'", con);
                    SqlCommand cmd1 = new SqlCommand("Select LookupId from lookUp where Category='STUDENT_STATUS' and Name='" + sts.SelectedItem + "'", con);
                    SqlDataAdapter da = new SqlDataAdapter(cmd1);
                    DataTable dt = new DataTable();
                    da.Fill(dt);
                    int lookid = Int32.Parse(dt.Rows[0].ItemArray[0].ToString());
                    cmd.Parameters.AddWithValue("@Status", lookid);
                    cmd.ExecuteNonQuery();


                    MessageBox.Show("Status Updated Successfully");
                }
            }
            else
            {
                MessageBox.Show("Please Enter student Id...");
            }
        }
    }
}
