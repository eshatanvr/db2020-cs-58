﻿using System.Data.SqlClient;
using System;
using System.Data;
using System.Windows.Forms;
using System.Configuration;

namespace WinFormsApp1
{
    public partial class Form6 : Form
    {
        public Form6()
        {
            InitializeComponent();
            comboBox_2();
            comboBox_3();
            comboBox_4();
        }
        Form2 f = new Form2();
        DateTime newDate;
        private void button1_Click(object sender, EventArgs e)
        {

            var conDel = Configuration.getInstance().getConnection();

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void button5_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 f = new Form1();
            f.Show();
        }

        private void dataGridView1_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {
            var conDel = Configuration.getInstance().getConnection();

            try
            {
                SqlCommand cmd1 = new SqlCommand("Delete from AssessmentComponent Where AssessmentId='" + AssID.SelectedItem + "'", conDel);

                cmd1.ExecuteNonQuery();

                MessageBox.Show("Successfully deleted from Assessment Components...");
            }
            catch
            {
                MessageBox.Show("can't found given data...");

            }
            SqlCommand cmd = new SqlCommand("Delete from Assessment Where Id='" + AssID.SelectedItem + "'", conDel);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully deleted from Assessments...");
        }

        private void button3_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Assessment", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button2_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd = new SqlCommand("Insert into Assessment values (@Title, @DateCreated, @TotalMarks,@TotalWeightage)", con);
            cmd.Parameters.AddWithValue("@Title", tit.Text);
            cmd.Parameters.AddWithValue("@DateCreated", newDate);
            cmd.Parameters.AddWithValue("@TotalMarks", TM.Text);
            cmd.Parameters.AddWithValue("@TotalWeightage", TW.Text);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved");
        }

        private void button1_Click_1(object sender, EventArgs e)
        {
            newDate = DateTime.Now;
            MessageBox.Show("Date added");
        }
        public void comboBox_1()
        {
            AssID.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Assessment", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                AssID.Items.Add(dt.Rows[i].ItemArray[0]);
            }
        }

        private void button6_Click(object sender, EventArgs e)
        {
            comboBox_1();
        }

        private void textBox1_TextChanged(object sender, EventArgs e)
        {

        }

        private void button7_Click(object sender, EventArgs e)
        {
            newDate = DateTime.Now;
            MessageBox.Show("Date added");
        }

        private void button11_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 f = new Form1();
            f.Show();
        }

        private void button9_Click(object sender, EventArgs e)
        
           
        {
            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd = new SqlCommand("Insert into AssessmentComponent values (@Name,@RubricId,@TotalMarks ,@DateCreated,@DateUpdated,@AssessmentId)", con);
            cmd.Parameters.AddWithValue("@Name", name.Text);
            SqlCommand cmd1 = new SqlCommand("Select Id from Rubric where Id= '" + ascorubID.SelectedItem + "' ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            da.Fill(dt);
            int id = Convert.ToInt32(dt.Rows[0].ItemArray[0].ToString());
            cmd.Parameters.AddWithValue("@RubricId", id);
            cmd.Parameters.AddWithValue("@TotalMarks", TM.Text);
            cmd.Parameters.AddWithValue("@DateCreated", newDate);
            cmd.Parameters.AddWithValue("@DateUpdated", newDate);
            SqlCommand cmd2 = new SqlCommand("Select Id from Assessment where Id= '" + AsscomID.SelectedItem + "' ", con);
            SqlDataAdapter daa = new SqlDataAdapter(cmd2);
            DataTable dtt = new DataTable();
            daa.Fill(dtt);
            int asId = Int32.Parse(dtt.Rows[0].ItemArray[0].ToString());
            cmd.Parameters.AddWithValue("@AssessmentId", asId);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully saved");
        }
        public void comboBox_2()
        {
            ascorubID.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Rubric", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                ascorubID.Items.Add(dt.Rows[i].ItemArray[0]);
            }
        }
        public void comboBox_3()
        {
            asscomIdDel.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from AssessmentComponent", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                asscomIdDel.Items.Add(dt.Rows[i].ItemArray[0]);
            }
        }

        private void button10_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from AssessmentComponent", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView2.DataSource = dt;
        }

        private void button8_Click(object sender, EventArgs e)
        {
            var conDel = Configuration.getInstance().getConnection();

            try
            {
                SqlCommand cmd1 = new SqlCommand("Delete from AssessmentComponent Where Id='" + asscomIdDel.SelectedItem + "'", conDel);

                cmd1.ExecuteNonQuery();

                MessageBox.Show("Successfully deleted from Assessment Components...");
            }
            catch
            {
                MessageBox.Show("can't found given data...");

            }
        }

        public void comboBox_4()
        {
            AsscomID.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Assessment", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                AsscomID.Items.Add(dt.Rows[i].ItemArray[0]);
            }
        }
        private void label10_Click(object sender, EventArgs e)
        {

        }

        private void button12_Click(object sender, EventArgs e)
        {
            comboBox_3();
        }
    }
}
