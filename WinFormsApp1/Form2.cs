﻿using System.Data.SqlClient;
using System;
using System.Data;
using System.Windows.Forms;
using System.Configuration;

namespace WinFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
            comboBox_1();
            comboBox_2();


        }

        /*  public int ID(int rubId)
          {
              rubId = rubId + 1;
              return rubId;
          }
          int rubId = 1;*/
        private void button1_Click(object sender, EventArgs e)
        {
            Form3 f = new Form3();

            var con = Configuration.getInstance().getConnection();

            SqlCommand cmd = new SqlCommand("Insert into Rubric values (@Id,@Details,@CloId) ", con);

            SqlCommand cmd1 = new SqlCommand("Select Id from Clo where Id= '" + comboBox1.SelectedItem + "' ", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd1);
            DataTable dt = new DataTable();
            da.Fill(dt);
            int id = Convert.ToInt32(dt.Rows[0].ItemArray[0].ToString());
            // MessageBox.Show(id.ToString());
            
           // int IDs = ID(rubId);
            try
            {
                cmd.Parameters.AddWithValue("@Id", rubid.Text);

                cmd.Parameters.AddWithValue("@Details", textBox1.Text.ToString());
                cmd.Parameters.AddWithValue("@CloId", id);
                cmd.ExecuteNonQuery();
                MessageBox.Show("Successfully saved");
            }catch(Exception ex)
            {
                MessageBox.Show("can not add duplicate id...");

            }

        }

        private void button2_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form2 f1 = new Form2();
            f1.Show();
        }

        private void button4_Click(object sender, EventArgs e)
        {
            var conDel = Configuration.getInstance().getConnection();

            try
            {
                SqlCommand cmd1 = new SqlCommand("Delete from RubricLevel Where RubricId='" + idDel.SelectedItem + "' and MeasurementLevel='" + mesID.SelectedItem + "'", conDel);

                cmd1.ExecuteNonQuery();

                MessageBox.Show("Successfully deleted from rubric level..");
            }
            catch
            {
                MessageBox.Show("can't found given data...");

            }
            SqlCommand cmd = new SqlCommand("Delete from Rubric Where Id='" + idDel.SelectedItem + "'", conDel);
            cmd.ExecuteNonQuery();
            MessageBox.Show("Successfully deleted from rubrics...");
        }

        private void button5_Click(object sender, EventArgs e)
        {
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select * from Rubric", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            dataGridView1.DataSource = dt;
        }

        private void button3_Click(object sender, EventArgs e)
        {
            this.Hide();
            Form1 f = new Form1();
            f.Show();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {

        }
        public void comboBox_1()
        {
                        comboBox1.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Clo", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++) 
            {
                comboBox1.Items.Add(dt.Rows[i].ItemArray[0]);
            }
        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void groupBox1_Enter(object sender, EventArgs e)
        {

        }
        public void comboBox_2()
        {
            idDel.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select Id from Rubric", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                idDel.Items.Add(dt.Rows[i].ItemArray[0]);
            }
        }
        public void comboBox_3()
        {
            mesID.Items.Clear();
            var con = Configuration.getInstance().getConnection();
            SqlCommand cmd = new SqlCommand("Select MeasurementLevel from RubricLevel where RubricId='" + idDel.SelectedItem + "'", con);
            SqlDataAdapter da = new SqlDataAdapter(cmd);
            DataTable dt = new DataTable();
            da.Fill(dt);
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                mesID.Items.Add(dt.Rows[i].ItemArray[0]);
            }
        }

        private void idDel_SelectedIndexChanged(object sender, EventArgs e)
        {
  

        }

        private void button6_Click(object sender, EventArgs e)
        {
            comboBox_3();
        }
    }
}
